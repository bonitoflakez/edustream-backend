import mongoose from "mongoose";
const Schema = mongoose.Schema;

const teacherSchema = new Schema({
  authId: { type: mongoose.Types.ObjectId, ref: "Auth", required: true },

  // General data
  about: { type: String },
  subjectsTeaching: [{ type: String }],
  hoursStreamed: { type: Number, default: 0 },
  studentsSubscribed: { type: Number, default: 0 },
  reviews: [
    {
      student: { type: mongoose.Types.ObjectId, ref: "Student" },
      rating: { type: Number, required: true },
      review: { type: String },
    },
  ],
  totalRatings: { type: Number, default: 0 },
  isOnline: { type: Boolean, default: false },
});

const Teacher = mongoose.model("Teacher", teacherSchema);

export default Teacher;

import mongoose from "mongoose";
const Schema = mongoose.Schema;

const studentSchema = new Schema({
  authId: { type: mongoose.Types.ObjectId, ref: "Auth", required: true },

  // General data
  about: { type: String },
  subjectsSubscribed: [{ type: String }],
  subscribedTeachers: [{ type: mongoose.Types.ObjectId, ref: "Teacher" }],
  hoursStudied: { type: Number, default: 0 },
  isOnline: { type: Boolean, default: false },
});

const Student = mongoose.model("Student", studentSchema);

export default Student;

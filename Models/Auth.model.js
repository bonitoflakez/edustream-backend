import mongoose from "mongoose";
const Schema = mongoose.Schema;

const authSchema = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  role: { type: String, required: true }, // 'teacher' or 'student'
});

const Auth = mongoose.model("Auth", authSchema);

export default Auth;

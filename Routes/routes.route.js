import express from "express";
import authRoutes from "./auth.route.js";
import healthCheck from "./healthCheck.route.js";

const router = express.Router();

router.use("/auth", authRoutes);
router.use("/check", healthCheck);

export default router;

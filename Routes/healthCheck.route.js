import express from "express";
import mongoose from "mongoose";

const router = express.Router();

router.get("/health", async (req, res) => {
  try {
    await mongoose.connection.db.admin().ping();

    res.status(200).json({
      message: "API and MongoDB working properly",
    });
  } catch (error) {
    res.status(500).json({ error: "Something's wrong with API or MongoDB" });
  }
});

export default router;

import express from "express";
const router = express.Router();

router.post("/register", (req, res) => {
  return res.status(200).json({
    message: "Registered successfully!",
  });
});

export default router;

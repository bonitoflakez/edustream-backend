import mongoose, { connect } from "mongoose";

const connectDB = () => {
  mongoose.connect(process.env.URI);
  const db = mongoose.connection;

  db.on("error", console.error.bind(console, "MongoDB connection error:"));
  db.once("open", () => {
    console.log("Connected to MongoDB");
  });

  return db;
};

export default connectDB;

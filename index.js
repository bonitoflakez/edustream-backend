import express from "express";

import allRoutes from "./Routes/routes.route.js";
import connectDB from "./Config/db.js";

const app = express();
const port = process.env.PORT || 3000;

const db = connectDB();

app.use(express.json());

app.use("/", allRoutes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
